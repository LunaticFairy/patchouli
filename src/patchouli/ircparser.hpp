#ifndef IRCPARSER_H_
#define IRCPARSER_H_

#define BUFSIZE 1024

#include <pcre.h>
#include <iostream>
#include <string.h>
#include <cstdio>

struct IRCData {
    IRCData() {}
    IRCData(std::string prefix, std::string command, std::string params, std::string trail) :
        prefix(prefix), command(command), params(params), trail(trail) {}
    std::string prefix;
    std::string command;
    std::string params;
    std::string trail;
};

struct UserData {
    UserData() {}
    UserData(std::string nick, std::string user, std::string host) :
        nick(nick), user(user), host(host) {}
    std::string nick;
    std::string user;
    std::string host;
};

class IRCParser
{
public:
    IRCParser(int bufsize = BUFSIZE);
    
    IRCData parseIRCData(std::string input);
    UserData parseUserData(std::string input);
private:
    int bufsize;
    void handleError(int error);
};

#endif // IRCPARSER_H_

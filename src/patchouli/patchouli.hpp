#ifndef PATCHOULI_H_
#define PATCHOULI_H_

#include <jsoncpp/json/json.h>
#include <libpq-fe.h>

#include <algorithm>
#include <string> 
#include <map>
#include <boost/algorithm/string.hpp>

#include "tcpclient.hpp"
#include "ircparser.hpp"
#include "pluginmanager.hpp"
#include "irccommands.hpp"

class Patchouli {
public:
    Patchouli(Json::Value root);
    ~Patchouli();

    void start();
    void handleData(std::string data);

    void addCTCPReply(std::string key, std::string data);
    std::string getCTCPReply(std::string key);
    void sendRaw(std::string what);
    PGconn* getDatabase(); 
    Json::Value getConfig();
    IrcCommands sendCommand();
    PluginManager* getPluginManager();
private:
    PluginManager plugins;
    Json::Value root;
    TCPClient client;
    IRCParser parser;
    IrcCommands commands;
    PGconn *db;
    bool firstRun;
    std::map <std::string, std::string> ctcpReply;
};

#endif

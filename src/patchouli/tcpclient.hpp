#ifndef TCPCLIENT_H_
#define TCPCLIENT_H_

#include <iostream>
#include <unistd.h>     //close()
#include <string.h>
#include <sys/socket.h> // 
#include <arpa/inet.h>  //
#include <netinet/in.h> //
#include <netdb.h>      // socket related 
#include <errno.h>      // error handling

class Patchouli;

struct ConnectionInfo {
    ConnectionInfo() {}
    ConnectionInfo(std::string address, int port) : address(address), port(port) {}
    std::string address;
    int port;
};

class TCPClient
{
public:
    TCPClient();
    ~TCPClient();
    bool connectToServer(ConnectionInfo info);
    void start(Patchouli* obj, void (Patchouli::*func)(std::string));
    void writeSocket(std::string data);
private:
    int fd;
    int bufpos;
    char* buf;
};

#endif // EVENTLOOP_H

#ifndef IRCCOMMANDS_H_
#define IRCCOMMANDS_H_

#include <iostream>
#include "tcpclient.hpp"
class IrcCommands {
public:
    IrcCommands(TCPClient* client);
    void sendRaw(std::string what);
    void sendMessage(std::string where, std::string message);
    void sendNotice(std::string where, std::string message);
    void act(std::string where, std::string message);
    void replyCTCP(std::string where, std::string data);
    void joinChannel(std::string channel, std::string key = std::string());
    void partChannel(std::string channel, std::string message = std::string());
    void setNick(std::string nick);
    void setUser(std::string nick, std::string user, std::string realname);
    void nickserv(std::string user, std::string pass);
private:
    TCPClient* client;
};

#endif

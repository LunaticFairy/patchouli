#include "irccommands.hpp"

IrcCommands::IrcCommands(TCPClient* client) {
    this->client = client;
}

void IrcCommands::sendRaw(std::string what) {
    client->writeSocket(what.append("\r\n"));
}

void IrcCommands::sendMessage(std::string where, std::string message) {
    this->sendRaw(std::string("PRIVMSG ").append(where).append(" :").append(message));
}

void IrcCommands::sendNotice(std::string where, std::string message) {
    this->sendRaw(std::string("NOTICE ").append(where).append(" :").append(message));
}

void IrcCommands::act(std::string where, std::string message) {
    this->sendMessage(where, std::string("\1").append("ACTION ").append(message).append("\1"));
}

void IrcCommands::replyCTCP(std::string where, std::string data) {
    this->sendRaw(std::string("NOTICE ").append(where).append(" :\1").append(data).append("\1"));
}

void IrcCommands::joinChannel(std::string channel, std::string key) {
    this->sendRaw(std::string("JOIN ").append(channel).append(" ").append(key.empty() ? "" : key));
}

void IrcCommands::partChannel(std::string channel, std::string message) {
    this->sendRaw(std::string("PART ").append(channel).append(message.empty() ? std::string(" :").append(message) : ""));
}

void IrcCommands::setNick(std::string nick) {
    this->sendRaw(std::string("NICK ").append(nick));
}

void IrcCommands::setUser(std::string nick, std::string user, std::string realname) {
    this->sendRaw(std::string("USER ").append(nick).append(" ").append(user).append(" * :").append(realname));
}

void IrcCommands::nickserv(std::string user, std::string pass) {
    this->sendRaw(std::string("NICKSERV IDENTIFY ").append(user).append(" ").append(pass));
}


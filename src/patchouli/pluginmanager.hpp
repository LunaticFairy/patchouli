#ifndef PLUGINMANAGER_H_
#define PLUGINMANAGER_H_

// Somehow, this macro isnt registered on my system.
//#ifndef RTLD_DI_LINKMAP
//#define RTLD_DI_LINKMAP 2
//#endif

#include <iostream>
#include <map>
#include <vector>
#include <thread>
#include <unistd.h>
#include <sys/inotify.h>
#include <dirent.h>
#include <link.h>
#include <dlfcn.h>

#include "ircparser.hpp"

class Patchouli;

typedef std::string (*fptr)(Patchouli*, IRCData, UserData, std::string, std::string);

class PluginManager {
public:
    PluginManager();
    ~PluginManager();
    void load(std::string name);
    void loadDir(std::string dir);
    std::string call(std::string command, std::string chain, std::string commandline, Patchouli* patchy, IRCData irc, UserData user);
    void unload(std::string name);
    void startWatcher(std::string dir);
    const std::vector<std::string> getSymbols(std::string name);
    const std::vector<std::string> getLibraries(std::string name);
    std::vector<std::string> getPlugins();
private:
    std::vector<std::string> readSymbols(void* handle);
    std::vector<std::string> readLibraries(void* handle);
    size_t findVal (ElfW(Dyn) *dyn, ElfW(Sxword) tag);
    void* findPtr(ElfW(Dyn)* dyn, ElfW(Sxword) tag);
    ElfW(Dyn)* findDyn(ElfW(Dyn)* dyn, ElfW(Sxword) tag);

    int inotify_fd;
    void filesystemWatcher(PluginManager* man);
    std::map<std::string, void*> plugins;
    std::map<std::string, std::vector<std::string>> symbols;
    std::map<std::string, std::vector<std::string>> libraries;
};

#endif

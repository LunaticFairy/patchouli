#include "pluginmanager.hpp"

PluginManager::PluginManager() {
    this->inotify_fd = -1;
}

PluginManager::~PluginManager() {
    if(this->inotify_fd != -1)
        close(this->inotify_fd);
}

void PluginManager::load(std::string name) {
    void* handle = dlopen(std::string("./plugins/").append(name).c_str(), RTLD_LOCAL | RTLD_NOW);
    if(!handle)
        std::cout << "Couldnt load " << name << ": " << dlerror() << std::endl;
    else { 
        plugins.insert(std::pair<std::string, void*>(name, handle));
        this->symbols.insert(std::pair<std::string,std::vector<std::string>>(name, this->readSymbols(handle)));
        this->libraries.insert(std::pair<std::string,std::vector<std::string>>(name, this->readLibraries(handle)));
    }
}

void PluginManager::loadDir(std::string dirpath) {
    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir(dirpath.c_str())) != NULL) {
        while ((ent = readdir (dir)) != NULL) {
            load(std::string(ent->d_name));
        }
        closedir(dir);
    }
}

void PluginManager::unload(std::string name) {
    if(plugins.count(name) > 0) {
        void *handle = plugins[name];
        plugins.erase(name);
        symbols.erase(name);
        dlclose(handle);
    }
}

std::string PluginManager::call(std::string command, std::string chain, std::string commandline, Patchouli* patchy, IRCData irc, UserData user) {
    for(std::map<std::string, void*>::iterator iterator = plugins.begin(); iterator != plugins.end(); iterator++) {
        fptr func = (fptr) dlsym(iterator->second, command.c_str());
        if(!dlerror())
            return func(patchy, irc, user, chain, commandline);
    }
    return chain;
}

void PluginManager::startWatcher(std::string dir) {
    this->inotify_fd = inotify_init();
    if (this->inotify_fd == -1)
        return;
    inotify_add_watch(this->inotify_fd, dir.c_str(), IN_ALL_EVENTS);
    std::thread t(&PluginManager::filesystemWatcher, this, this);
    t.detach();
}

void PluginManager::filesystemWatcher(PluginManager* man) {
    char buf[128];
    int ev;
    for(;;) {
        ev = read(this->inotify_fd, buf, 128);
        if(ev == 0)
            std::cerr << "read() returned 0" << std::endl;
        for(char* p = buf; p < buf + ev; ) {
            inotify_event *event = (inotify_event*) p;
            if(event->mask & IN_CREATE)
                man->load(event->name);
            else if(event->mask & IN_DELETE)
                man->unload(event->name);
            else if(event->mask & IN_DELETE_SELF) 
                man->unload(event->name);
            else if(event->mask & IN_MOVED_FROM)
                man->unload(event->name);
            else if(event->mask & IN_MOVED_TO)
                man->load(event->name);
            else if(event->mask & IN_MODIFY) {
                man->unload(event->name);
                man->load(event->name);
            }
            p += sizeof(inotify_event) + event->len;
        }
   } 
}

std::vector<std::string> PluginManager::readSymbols(void* handle) {
    std::vector<std::string> symbols;
    struct link_map* map = 0;

    if(dlinfo(handle, RTLD_DI_LINKMAP, &map)){
        printf("Failed: %s", dlerror());
        return symbols;
    }
    ElfW(Dyn)* dyn_start = map->l_ld;

    size_t symentsize = findVal(dyn_start, DT_SYMENT);
    ElfW(Sym)* symtab = (ElfW(Sym)*)findVal(dyn_start, DT_SYMTAB);
    ElfW(Sym)* syment = (ElfW(Sym)*)symtab;
    if(symtab == NULL) {
        return symbols;
    }
    char* strtab = (char*) findPtr(dyn_start, DT_STRTAB);

    while(syment != NULL) {
        if(syment->st_name > 0x1000000)
            break;
        if(syment->st_size) {
            symbols.push_back(&strtab[syment->st_name]);
        }
        syment = (ElfW(Sym)*)((uint8_t*)(syment) + symentsize);
    }

    return symbols;
}

std::vector<std::string> PluginManager::readLibraries(void* handle) {
    std::vector<std::string> libs;
    struct link_map* map = 0;

    if(dlinfo(handle, RTLD_DI_LINKMAP, &map)) {
        printf("Failed: %s", dlerror());
        return libs;
    }

    ElfW(Dyn)* dyn_start = map->l_ld;
    size_t strtabsize = findVal(dyn_start, DT_STRSZ);
    char* strtab = (char*) findPtr(dyn_start, DT_STRTAB);
    for (ElfW(Dyn)* dyn = findDyn(dyn_start, DT_NEEDED); dyn; dyn = findDyn((ElfW(Dyn)*)++dyn, DT_NEEDED)) {
        const size_t index = dyn->d_un.d_val;
        if(index < strtabsize){
            libs.push_back(std::string().assign(&strtab[index]));
        }
    }
    return libs;
}

size_t PluginManager::findVal(ElfW(Dyn) *dyn, ElfW(Sxword) tag) {
    for(; dyn->d_tag != DT_NULL; ++dyn) {
        if(dyn->d_tag == tag)
            return dyn->d_un.d_val;
    }
    return 0;
}

void* PluginManager::findPtr(ElfW(Dyn)* dyn, ElfW(Sxword) tag) {
    for (; dyn->d_tag != DT_NULL; ++dyn) {
        if (dyn->d_tag == tag)
            return (void*)(dyn->d_un.d_ptr);
    }
    return 0;
}

ElfW(Dyn)* PluginManager::findDyn(ElfW(Dyn)* dyn, ElfW(Sxword) tag) {
    for (; dyn->d_tag != DT_NULL; ++dyn) {
        if (dyn->d_tag == tag)
            return dyn;
    }
    return 0;
}


const std::vector<std::string> PluginManager::getSymbols(std::string name) {
    return this->symbols[name];
}

const std::vector<std::string> PluginManager::getLibraries(std::string name) {
    return this->libraries[name];
}

std::vector<std::string> PluginManager::getPlugins() {
    std::vector<std::string> plugs;
    for(std::map<std::string, void*>::iterator it = plugins.begin(); it != plugins.end(); ++it) 
        plugs.push_back(it->first);
    return plugs;
}

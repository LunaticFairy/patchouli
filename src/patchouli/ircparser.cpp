#include "ircparser.hpp"

IRCParser::IRCParser(int bufsize) {
    this->bufsize = bufsize;
}

IRCData IRCParser::parseIRCData(std::string input) {
    IRCData data;
    
    int subStrVec[this->bufsize];
    try {
        const char *regex = "^(:(?<prefix>\\S+) )?(?<command>\\S+)( (?!:)(?<params>.+?))?( :(?<trail>.+))?\r";

        int erroffset = 0;
        const char *err;
        const char *pcreErrorStr;

        pcre *re = pcre_compile(regex, 0, &err, &erroffset, (char)PCRE_UTF8);
        if(re == NULL && err != 0) {
            printf("ERROR: Could not compile '%s': %s\n", regex, pcreErrorStr);
            pcre_free(re);
            return data;
        }

        pcre_extra *pcreExtra = pcre_study(re, 0, &pcreErrorStr);
        if(pcreErrorStr != NULL) {
            printf("ERROR: Could not study '%s': %s\n", regex, pcreErrorStr);
            pcre_free(re);
            if(pcreExtra != NULL) pcre_free(pcreExtra);
            return data;
        }

        int pcreExecRet = pcre_exec(re, pcreExtra, input.c_str(), strlen(input.c_str()), 0, 0, subStrVec, 1024);
        if(pcreExecRet < 0)
            handleError(pcreExecRet);
        else {
            if(pcreExecRet == 0) {
                printf("But too many substrings were found to fit in subStrVec!\n");
                pcreExecRet = strlen(input.c_str()) / 4;
            }
            const char *psubPrefix = NULL, *psubCommand = NULL, *psubParams = NULL, *psubTrail = NULL;
            pcre_get_named_substring(re, input.c_str(), subStrVec, pcreExecRet, "prefix", &(psubPrefix));
            pcre_get_named_substring(re, input.c_str(), subStrVec, pcreExecRet, "command", &(psubCommand));
            pcre_get_named_substring(re, input.c_str(), subStrVec, pcreExecRet, "params", &(psubParams));
            pcre_get_named_substring(re, input.c_str(), subStrVec, pcreExecRet, "trail", &(psubTrail));

            data.prefix = (psubPrefix == NULL) ? std::string() : std::string(psubPrefix); 
            data.command = (psubCommand == NULL) ? std::string() : std::string(psubCommand); 
            data.params = (psubParams == NULL) ? std::string() : std::string(psubParams); 
            data.trail = (psubTrail == NULL) ? std::string() : std::string(psubTrail);

            pcre_free_substring(psubPrefix);
            pcre_free_substring(psubCommand);
            pcre_free_substring(psubParams);
            pcre_free_substring(psubTrail);
        }

        pcre_free(re);
        if(pcreExtra != NULL) pcre_free_study(pcreExtra);
    } catch(std::exception e) {
        std::cerr << "Parser fault: " << e.what() << std::endl;
    }
    
    return data;
}

UserData IRCParser::parseUserData(std::string input) {
    UserData data;
    int nickpos = input.find("!");
    int addresspos = input.find("@");
    data.nick = input.substr(1, nickpos - 1);
    data.user = input.substr(nickpos + 1, addresspos - nickpos - 1);
    data.host = input.substr(addresspos + 1, input.length());
    return data;
}

void IRCParser::handleError(int error) {
    switch(error) {
        case PCRE_ERROR_NOMATCH      : printf("String did not match the pattern\n");        break;
        case PCRE_ERROR_NULL         : printf("Something was null\n");                      break;
        case PCRE_ERROR_BADOPTION    : printf("A bad option was passed\n");                 break;
        case PCRE_ERROR_BADMAGIC     : printf("Magic number bad (compiled re corrupt?)\n"); break;
        case PCRE_ERROR_UNKNOWN_NODE : printf("Something kooky in the compiled re\n");      break;
        case PCRE_ERROR_NOMEMORY     : printf("Ran out of memory\n");                       break;
        case PCRE_ERROR_BADUTF8      : printf("Bad UTF-8 data\n");                          break;
        default                      : printf("Unknown error\n");                           break;
    }
}

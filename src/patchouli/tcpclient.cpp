#include "tcpclient.hpp"

TCPClient::TCPClient() {
    buf = new char[1024];
}

TCPClient::~TCPClient() {
    delete[] buf;
}

bool TCPClient::connectToServer(ConnectionInfo info) {
    if(-1 == (this->fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP))){
        std::cerr << "Socket creation fault" << std::endl;
        return false;
    }

    hostent *record = gethostbyname(info.address.c_str());
    if(record == NULL) {
        return false;
    }

    struct sockaddr_in sock_info;
    
    memset((void *) &sock_info, 0, sizeof(struct sock_info*));
    memcpy((char *) &sock_info.sin_addr, record->h_addr, record->h_length); 
    sock_info.sin_family = AF_INET;
    sock_info.sin_port = htons(info.port);
      
    if(-1 == connect(fd, (struct sockaddr*) &sock_info, sizeof(sockaddr_in))) {
        std::cerr << "Socket connection fault: " << strerror(errno)  << std::endl;
        close(fd);
        return false;
    }
    return true;
}

void TCPClient::start(Patchouli* obj, void (Patchouli::*func)(std::string)) {
    while(1) {
        int bytes = read(fd, buf + bufpos, 1024 - bufpos);

        int i = bufpos;
        int end = -1;
        for(; i < bytes + bufpos; i++)
        {
            if(buf[i] == '\n')
            {
                buf[i] = 0;
                ((obj)->*(func))(buf + end + 1);
                end = i;
            }
        }
    
        if(end != -1)
        {
            memmove(buf, buf + end + 1, i - end - 1);
            bufpos = i - end - 1;
        }

    }
}

void TCPClient::writeSocket(std::string data) {
    send(fd, data.c_str(), data.size(), 0);
}

#include "patchouli.hpp"

Patchouli::Patchouli(Json::Value root) : commands(&client){
    this->root = root;
    this->client = TCPClient();
    this->firstRun = true;
}

Patchouli::~Patchouli() {
    PQfinish(this->db);
}

void Patchouli::start() {
    this->addCTCPReply("version", "VERSION Patchouli v1.0");
    plugins.loadDir("./plugins/");
    plugins.startWatcher("./plugins/");

    std::string address = root["irc"].get("address","irc.esper.net").asString();
    int port = root["irc"].get("port", 6667).asInt();
    client.connectToServer(ConnectionInfo(address, port));
    client.start(this, &Patchouli::handleData);
}

void Patchouli::handleData(std::string data) {
    std::cout << data << std::endl << std::endl;
    IRCData parsed = parser.parseIRCData(data);
    UserData user = parser.parseUserData(data);

    if(firstRun == true) {
        // I have no idea why i have to put this here. If i don't, the connection doesnt seem to work.
        this->db = PQconnectdb(
            std::string("hostaddr=").append(root["database"].get("address", "127.0.0.1").asString())
            .append(" port=").append(std::to_string(root["database"].get("port", 5432).asInt()))
            .append(" dbname=").append(root["database"].get("dbname", "patchouli").asString())
            .append(" user=").append(root["database"].get("username", "patchouli").asString())
            .append(" password=").append(root["database"].get("password", "patchouli").asString())
            .c_str()
        );
        if (PQstatus(this->db) != CONNECTION_OK) {
            std::cerr << "Connection to database failed: " << std::string(PQerrorMessage(this->db)) << std::endl;
            std::cerr << "Close the bot now, and fix." << std::endl;
            PQfinish(this->db);
        }
        this->sendCommand().setUser(
            root["irc"].get("nickname", "PatchouliKnowledge").asString(),
            root["irc"].get("username", "Patchouli").asString(),
            root["irc"].get("realname", "Patchouli Knowledge").asString()
        );
        this->sendCommand().setNick(root["irc"].get("nickname", "PatchouliKnowledge").asString());
        firstRun = false;
    }
    if(!parsed.command.empty()) {
        std::string lowerCmd = boost::to_lower_copy(parsed.command);
        if(parsed.command == "PING")
            client.writeSocket(std::string("PONG ").append(data.substr(5, data.length())));
        if(!parsed.trail.empty()) {
            std::string prefix = root["irc"].get("prefix", "%").asString();
            if(boost::starts_with(parsed.trail, prefix)) {
                std::string cmd = parsed.trail.substr(1, parsed.trail.length());
                std::vector<std::string> result;
                std::string chaindata;

                boost::split(result, cmd, boost::is_any_of("|"), boost::token_compress_on);
                for(std::vector<std::string>::iterator it = result.begin(); it != result.end(); it++) {
                    std::string iter = *it;
                    if(iter.empty()) 
                        continue;

                    std::string pcmd = boost::trim_left_copy(iter);
                    if(boost::find_first(pcmd, " ")) {
                        pcmd = pcmd.substr(0, pcmd.find(' '));
                    }
                    boost::to_lower(pcmd);
                    
                    if(boost::ends_with(pcmd, "?")) {
                        chaindata = plugins.call(std::string("command_").append(pcmd.substr(0, pcmd.length()-1)), chaindata, iter, this, parsed, user);
                        this->sendCommand().sendMessage(parsed.params, chaindata);
                    } 
                    else chaindata = plugins.call(std::string("command_").append(pcmd), chaindata, iter, this, parsed, user);
                }
            }
            if(parsed.trail[0] == '\1') {
                std::string ctcp = parsed.trail.substr(1, parsed.trail.length() - 2);
                boost::to_lower(ctcp);
                
                std::string reply = this->getCTCPReply(ctcp);
                if(!reply.empty()) {
                    this->sendCommand().replyCTCP(user.nick, reply);
                }
            }
        }
        if(parsed.command == "376") {
            this->sendCommand().joinChannel(root["irc"].get("channel","#test").asString());
            this->sendCommand().nickserv(root["irc"].get("nickname", "").asString(), root["irc"].get("nickserv","").asString());
        }

        plugins.call(std::string("hook_").append(lowerCmd), "", "", this, parsed, user);
    }
}

void Patchouli::addCTCPReply(std::string key, std::string data) {
    ctcpReply.insert(std::pair<std::string, std::string>(key, data));
}

std::string Patchouli::getCTCPReply(std::string key) {
    return (ctcpReply.count(key) > 0) ? ctcpReply[key] : std::string();
}

PGconn* Patchouli::getDatabase() {
    return this->db;
}

Json::Value Patchouli::getConfig() {
    return this->root;
}

IrcCommands Patchouli::sendCommand() {
    return this->commands;
}

PluginManager* Patchouli::getPluginManager() {
    return &this->plugins;
}

void Patchouli::sendRaw(std::string what) {
    client.writeSocket(what.append("\r\n"));
}

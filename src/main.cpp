#include <iostream>
#include <fstream>
#include <jsoncpp/json/json.h>

#include "patchouli/patchouli.hpp"

int main() {
    std::ifstream configFile("config.json", std::ifstream::in);
    Json::Reader reader;
    Json::Value root;
    reader.parse(configFile, root, false);
    Patchouli patchy(root);
    patchy.start();
}
